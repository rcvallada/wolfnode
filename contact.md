---
title: Contact
---

<h1>Contact</h1>

## Formal Contact

### Email

Send emails to **rvallada@ucsc.edu.** This is my **preferred way** of 
communication with people I don't know all to well. 

If I don't know who you are and you attempt contacting me by informal mediums, 
you will most likely be ignored, or worse, blocked :(

### Skype

I don't use skype primarily anymore. Please consider using google hangouts if
a possibility. If you for whatever reason **need** me on skype (potential
employer maybe) send me an email and I will gladly share my username.

## Informal Contact

### Discord

Add me by my tag, **Wolf#8413**. If I'm not glaring at email then I'm glaring at
this.
