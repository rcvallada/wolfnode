---
title: Resume
---
<h1>Resume</h1>
<div class="info"> Last Updated: May 31, 2017</div>
<div class="info"><a href="./doc/resume.pdf" download>Download Copy</a></div>
<div id="resume">
<embed src="./doc/resume.pdf" width="100%" height="800px"/>
</div>