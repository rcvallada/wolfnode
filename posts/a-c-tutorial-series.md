---
title: A C Tutorial Series 
date: 2017-04-19
post-id: a-c-tutorial-series
---

I'm going to be writing a small C tutorial series for the beginning programmer.
I will do my best to explain the language in a way someone who has never
programmed before will understand, but it is still recommended to have some
programming experience.

I will tailor some parts to fit in line with classes at UCSC, so if you're a student
there, you'll probably come away from the series with some more value. Keep in
mind that this is my first ever genuine attempt at creating a tutorial series.
It'll be a great way to reinforce my knowledge of C, and find a suitable
teaching style at the same time.

I'll hope to have tags and disqus comments implemented soon for future posts, to
make it easier for future viewers to get around, if any come by per chance.
