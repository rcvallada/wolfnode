---
title: We're Live
date: 2017-04-21
post-id: we-re-live
---

I've finally got my personal blog site up! It took me a while to decide on which
web framework to use, but in the end I've stuck with Haskell+Hakyll. So far the
experience has been great. I've got just enough control over some dynamics
without having too much dynamics (if that makes any sense).

## The Blog

I actually don't know what I'll blog about yet. I'm in the process of figuring
out what tags to create for my posts. I feel once I have that out of the way
I'll be able to generate some content. For the meantime I'll blog about the
website's progress.

### Tags

Speaking of tags, I'll be working on those soon. Basically I'll be able to
categorize my posts so that viewers can find a post or search for content to
read in a more convenient way. 

### Why Haskell and Hakyll

I wanted to learn more about Haskell this upcoming summer, so in an attempt to
do as much as I can with Haskell, I've decided to stick with it for my blog.
Hakyll because static website generators are simple, secure and flexible enough 
for what I'll be doing.

### Disqus

I've set up Disqus for comments which I think is pretty neat. Hopefully anybody
that reads around here (if ever) will say a thing or two. Hopefully; a man can
dream.

### What's in Store for the Near Future

The only other post I've got around here talks about a C tutorial series. I will
be doing my best to go through with that. Other than that, I'll probably be
blogging about projects, UCSC and maybe things related to gaming. Only time will
tell.

Here's a small code snippet test for myself.

<script src="https://gist.github.com/anonymous/f532be89c6cf5c9348d80097679e2c05.js"></script>

## Meanwhile at HQ

I'll be working on the aesthetics of the website and will be tidying things up.
The current site design I've got going seems to be doing the trick so far. It is
simple and unobtrusive and I consider that a plus. If you got any ideas for 
design I'll be more than happy to hear of them.