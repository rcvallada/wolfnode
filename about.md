---
title: About
---

<h1>About</h1>

### The Name's Rodolfo

But you can call me Rudy. I'm a UCSC student majoring in Computer Science with
a minor in Computer Engineering. It is not official if that's what my degree's
fine print will be, but time will tell.

I enjoy programming and learning a lot of things. So many things that I will not
bother to list them. But to sum it up, here's a condensed list:

- Operating Systems
- Web Development
- Networking
- Parsing
- And algorithms of course.

### What I do

I usually just study. Therefore I'm very picky about any projects I currently
work on. When I do decide to pick up a pet project and 'maintain' it, I make
sure that I'll learn something new and/or improve something of my skillset that
is lacking.

Sadly this means I don't contribute much to open source as I would like to. I
also enjoy doubting myself as well and it doesn't help when I want to feel 
confident about my code. But I consider this a big plus as a student; I am
always striving to produce better code.

Since most of my projects deal with something I am inexperienced in, I have a
bit of trouble resuming their development. Because of this, my motivation tends
to ping pong among my projects. This also means however, that I am more open to
applying new technologies to my projects (as I do not have a need to consider
backwards compatiblity). This helps me to continue working on my projects even
after I've called it quits.

### What I Really Do

I sit in front of a monitor writing about things when I really should just be
studying.

### "I Doubt the Credit of Your Tutorials"

Bah, I do too. That's why I got that fancy disqus commentary stuff going. People
tell me I'm a pretty good 'teacher.' So I said fu** it, why not write some
tutorial series and reinforce my learning while others get to learn new things!
It is a win win in my opinion.  That rhymed a little.

Anyhow, try them out and if they end up being useless to you, let me know why
in the comments. If you have a contradictory case, also post that. I'd like to
hear both positives and negatives!